import hibernate.models.Reservation;
import hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import javax.persistence.Query;
import javax.persistence.Tuple;
import java.text.SimpleDateFormat;
import java.util.*;

public class Planning {

        public static void main(String[] args) {
            SessionFactory sf = new HibernateUtil().buildSessionFactory();
            Session session = sf.getCurrentSession();

            Transaction tx = session.beginTransaction();

            System.out.println("Saisissez la date souhaité (YYYY-MM-DD)");
            Scanner scanner = new Scanner(System.in);

            // On demande une saisie à l'utilisateur
            String dateSaisie = scanner.nextLine();
            Date dateObject = null;

            try {
                // On transforme en date
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                dateObject = dateFormat.parse(dateSaisie);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // On recherche les reservations acceptés sur cette date
            String hql = "SELECT r.lane, r.column " +
                    "FROM Reservation r " +
                    "WHERE r.dateReservation = :dateResa AND r.isAccepted = true";

            Query query = session.createQuery(hql, Tuple.class);
            query.setParameter("dateResa", dateObject);

            List<Tuple> resas = query.getResultList();

            // On parcours les 8 lignes possibles
            for (int i = 1; i<9; i++){
                // Je parcours toutes les colonnes
                for (int j = 1; j<37; j++){
                    // Par défaut la place est libre
                    boolean reserved = false;
                    // Je parcours mes reservations du jour
                    for (Tuple elem:resas) {
                        // Si j'ai un résultat avec la colonne égale à j et la ligne égale à j
                        if(elem.get(0).toString().equals( String.valueOf(i)) && elem.get(1).toString().equals(String.valueOf(j))){
                            // Ceci implique que l'emplacement est reservé pour la journée
                            reserved = true;
                            // Si il est reservé, je sors de ma boucle for
                            break;
                        }
                    }

                    if(reserved){
                        // Si mon emplacement est reservé, j'affiche X
                        System.out.print("X");
                    }else {
                        // Sinon j'affiche 0
                        System.out.print("O");
                    }
                }
                // Je fais un retour à la ligne (je passe sur une nouvelle ligne de la plage)
                System.out.println("\n");
            }

            tx.commit();
            session.close();
            sf.close();
        }
}
