import hibernate.models.Command;
import hibernate.models.Reservation;
import hibernate.models.User;
import hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        User mylene = session.load( User.class,Long.valueOf("1"));

        List<Command> myleneCommandes = mylene.getCommandes();

        for (Command cmd: myleneCommandes){
            System.out.println("Une commande de Mylène");
            List<Reservation> myleneResas = cmd.getReservations();
            for (Reservation resa:myleneResas){
                System.out.println(resa.getLane());
                System.out.println(resa.getColumn());
            }
        }

        tx.commit();
        session.close();
        sf.close();
    }
}
