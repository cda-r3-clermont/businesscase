import hibernate.models.Command;
import hibernate.models.Reservation;
import hibernate.models.User;
import hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Commande {
    public static void main(String[] args) {
        // Réccupére ma session factory
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();

        // Je démarre une nouvelle transaction
        Transaction tx = session.beginTransaction();

        //
        // Chargement de l'utilisateur qui fait la commande
        //
        User utilisateur = session.load(User.class, 1L);

        //
        // Je demande les saisies utilisateurs
        //
        System.out.println("Saisissez la date souhaité d'arriver (YYYY-MM-DD)");
        Scanner scanner = new Scanner(System.in);

        String dateArriverString = scanner.nextLine();
        Date dateObject = null;

        System.out.println("Saisissez la date souhaité de départ (YYYY-MM-DD)");
        String dateDepartString = scanner.nextLine();

        System.out.println("Sur quelle ligne souhaitez vous vous positionner ?");
        String ligne = scanner.nextLine();

        System.out.println("Combien de parasol voulez vous ?");
        String nbParasol = scanner.nextLine();


        System.out.println("Quelle option souhaitez vous ?");
        System.out.println("1 - Lit");
        System.out.println("2 - Parasol");
        System.out.println("3 - Bouteille de champagne");
        String option = scanner.nextLine();


            // Je cré un objet SimpleDateFormat pour transformer les dates en string OU les string en date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


            Calendar calArrivee = Calendar.getInstance();
            Calendar calSortie = Calendar.getInstance();

            try{
                //Définir la date
                calArrivee.setTime(sdf.parse(dateArriverString));
                calSortie.setTime(sdf.parse(dateDepartString));
            }catch(ParseException e){
                e.printStackTrace();
            }


            // Je cré une nouvelle commande
            Command command = new Command();
            command.setUser(utilisateur);
            command.setPayment(false);
            session.persist(command);

            String stringDateArrivee = "1";
            String stringDateDepart = sdf.format(calSortie.getTime());

            // Tant que la date d'arrivée n'est pas égale à la date de sortie
            // J'ajoute un jour à ma date d'arrivée
        while (!stringDateArrivee.equals(stringDateDepart))
            {
                calArrivee.add(Calendar.DAY_OF_MONTH, 1);
                stringDateArrivee = sdf.format(calArrivee.getTime());

                for(int i = 0; i< Integer.valueOf(nbParasol);i++){
                    Reservation resa = new Reservation();
                    resa.setCommand(command);
                    resa.setAccepted(false);
                    resa.setLane(Integer.valueOf(ligne));
                    resa.setEquipment(option);
                    resa.setDateReservation(calArrivee.getTime());
                    command.addReservation(resa);
                    session.persist(resa);
                }
            }
        tx.commit();

    }
}
