package hibernate.models;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, nullable = false)
    @NotBlank(message = "Veuillez saisir un email")
    @Email(message = "Veuillez entrer un email valide")
    private String email;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un mot de passe")
    private String password;

    @Basic
    private String role;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir une date")
    @Temporal(TemporalType.DATE)
    private Date dateAdd;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un pays")
    private String country;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir votre prénom")
    private String firstname;

    @Basic
    @NotBlank(message = "Veuillez saisir votre nom")
    private String lastname;

    @OneToMany(mappedBy = "user")
    private List<Command> commandes;

    public User() {
        this.commandes = new ArrayList<Command>();
    }

    public void addCommande(Command command){
        this.commandes.add(command);
    }

    public void removeCommande(Command command){
        this.commandes.remove(command);
    }
    public List<Command> getCommandes(){
        return this.commandes;
    }

    public Date getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(Date dateAdd) {
        this.dateAdd = dateAdd;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPassword() {
        return password;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
