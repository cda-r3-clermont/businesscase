package hibernate.models;

import jakarta.validation.constraints.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "command")
public class Command {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = true)
    private String remarque;

    @Column(nullable = false)
    @NotNull(message = "Veuillez indiquer si la commande est payée")
    private Boolean payment = false;

    @OneToMany(mappedBy = "command")
    private List<Reservation> reservations;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

    public Command() {
        this.reservations = new ArrayList<Reservation>();
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public Boolean getPayment() {
        return payment;
    }

    public void setPayment(Boolean payment) {
        this.payment = payment;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }



    public void addReservation(Reservation reservation){
        this.reservations.add(reservation);
    }

    public void removeReservation(Reservation reservation){
        this.reservations.remove(reservation);
    }

    public List<Reservation> getReservations(){
        return this.reservations;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
